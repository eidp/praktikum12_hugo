#include <iostream>
#include <fstream>
#include <stdexcept>
#include "portable_bitmap.h"


using namespace std;


bool transition(bool left, bool center, bool right);


int main()
{
    ofstream f;
    f.open("istmirscheissegalnennswieduwillst.txt");

    if (!f.is_open()) {
        cerr << "[!] Mein Leben ist doof.";
        exit(1);
    }

    char const characterMap[] = { '_', '#' };
    int const maxGenerations = 150;
    int const cellCount = 100;

    bool universe[cellCount] =
        { 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 };
    bool tempUniverse[cellCount] = { 0 };

    PortableBitmap map(maxGenerations, cellCount);
    for (int i = 0; i < maxGenerations; ++i) {
        // output
        for (int j = 0; j < cellCount; ++j) {
            map.setPixel(universe[j], j, i);
            f << characterMap[universe[j]];
            cout << characterMap[universe[j]];
        }
        f << endl;
        cout << endl;

        // update
        tempUniverse[0] =
            transition(universe[cellCount - 1], universe[0], universe[1]);
        for (int j = 1; j < cellCount - 1; ++j) {
            tempUniverse[j] =
                transition(universe[j - 1], universe[j], universe[j + 1]);
        }
        tempUniverse[cellCount - 1] =
            transition(universe[cellCount - 2], universe[cellCount - 1], universe[0]);
        // copy
        for (int j = 0; j < cellCount; ++j) {
            universe[j] = tempUniverse[j];
        }
    }
    
    try {
        map.save("datei.pbm");
        load("datei.pbm").save("datei2.pbm");
    } catch (invalid_argument &a) {
        cerr << a.what() << endl;
    } catch (runtime_error& a) {
        cerr << a.what() << endl;
    }

    f.close();

    return 0;
}


bool transition(bool left, bool center, bool right)
{
    static bool const transitionMap[] = { 0, 1, 1, 1, 0, 1, 1, 0 };

    int inputEncoded = right + 2 * center + 4 * left;
    return transitionMap[inputEncoded];
}

