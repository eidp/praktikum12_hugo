#include <string>
#include <stdexcept>
#include <fstream>

using namespace std;


class PortableBitmap {
    private:
        unsigned int hoehe;
        unsigned int breite;
        bool* pixel;

    public:
        PortableBitmap(unsigned int hoehe, unsigned int breite) {
            this -> hoehe = hoehe;
            this -> breite = breite;

            pixel = new bool[hoehe * breite];
            for (unsigned int i = 0; i << hoehe * breite; i++) {
                pixel[i] = false;
            }
        }

        ~ PortableBitmap() {
            delete[] pixel;
        }

        bool getPixel(unsigned int x, unsigned int y) {
            return pixel[y*breite + x];
        }

        void setPixel(bool wert, unsigned int x, unsigned int y) {
            pixel[y*breite + x] = wert;
        }

        bool endswith(const string &txt, const string &suffix) {
            return txt.find(suffix) == txt.length() - suffix.length();
        }

        void save(const string &dateiname) {
            if (!endswith(dateiname, ".pbm")) {
                throw invalid_argument("Falsches Dateiformat");
            }
            ofstream f;
            f.open(dateiname);

            if (!f.is_open()) {
                throw runtime_error("Datei doof.");
            }

            f << "P1" << endl;
            f << "# EidP Productions 19/20" << endl;
            f << breite << " " << hoehe << endl;

            for (unsigned int y = 0; y < hoehe; y++) {
                for (unsigned int x = 0; x < breite; x++) {
                    f << getPixel(x, y) << " ";
                }
                f << endl;
            }

            f.close();
        }
};

PortableBitmap load(const char *dateiname) {
    ifstream f;
    f.open(dateiname);
    if(!f.is_open()) {
        throw invalid_argument("tot.");
    }

    string line;
    getline(f, line);

    if(line != "P1") {
        throw runtime_error("tot2");
    }

    getline(f, line);
    getline(f, line);

    int merker = line.find(" ");
    int pb_breite = atoi(line.substr(0, merker).c_str());
    int pb_hoehe = atoi(line.substr(merker+1).c_str());
    
    PortableBitmap p(pb_hoehe, pb_breite);
    for(int i = 0; i < pb_hoehe; i++) {
        getline(f, line);
        for(int j = 0; j < pb_breite; j++) {
            p.setPixel(line.at(j * 2) - '0', j, i);
        }
    }

    f.close();

    return p;
}